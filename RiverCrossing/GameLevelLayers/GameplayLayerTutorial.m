//
//  GameplayBlock.m
//  IceSliding
//
//  Created by Jean-Jean Wei on 2016-12-14.
//  Copyright © 2016 Ice Whale Inc. All rights reserved.
//

#import "GameplayLayerTutorial.h"

#import "Tile1x1.h"


#import "Unit.h"
#import "Accessary.h"
#import "Layout.h"

#import "Flash1x1.h"
#import "Arrow.h"

#import "LevelHelper.h"
#import "TileHelper.h"

//#import "BlockMapping.h"

@implementation GameplayLayerTutorial

#define OFFX = boundingBox.size.width/2
#define OFFY = boundingBox.size.height/2


#define empty = @"-1";
#define block_1_1_9 = @"9";
#define block_1_1_0 = @"0";



#pragma mark –
#pragma mark Update Method
-(void) update:(ccTime)deltaTime
{
    if ([sceneSpriteBatchNode1 getChildByTag:kSpriteToDestoryTagValue] != nil)
    {
        [sceneSpriteBatchNode1 removeChildByTag:kSpriteToDestoryTagValue cleanup:true];
    }
    if ([sceneSpriteBatchNode getChildByTag:kSpriteToDestoryTagValue] != nil)
    {
        [sceneSpriteBatchNode removeChildByTag:kSpriteToDestoryTagValue cleanup:true];
    }
    CCArray *listOfGameObjects = [sceneSpriteBatchNode children]; //1
    CCArray *listOfGameObjects1 = [sceneSpriteBatchNode1 children]; //1
    for (GameCharacter *tempChar1 in listOfGameObjects1)
    {         // 2
    
        [tempChar1 updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects1];                         // 3
    }
    for (GameCharacter *tempChar in listOfGameObjects)
    {         // 2
        [tempChar updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects];                         // 3
    }

}

-(id) init
{
    if((self = [super init]))
    {
        [self playBackgroundMusic];
        
        srandom((int)time(NULL)); // Seeds the random number generator
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"accessary.plist"]; // 1
        sceneSpriteBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"accessary.png"]; // 2
        [self addChild:sceneSpriteBatchNode z:1]; // 3
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"layout.plist"]; // 1
        sceneSpriteBatchNode1 = [CCSpriteBatchNode batchNodeWithFile:@"layout.png"]; // 2
        [self addChild:sceneSpriteBatchNode1 z:0];
        
        [self createLevel];
        
        [self scheduleUpdate];
        
        // enable touches
        //  [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
        
        }
    return self;
}

- (void)playBackgroundMusic
{
    int level = GameManager.instance.currentLevel;
    
    if (level > 17)
        {
        [SoundManager.instance playBackgroundTrack:BACKGROUND_TRACK_EXP];
        }
    else if (level > 11)
        {
        [SoundManager.instance playBackgroundTrack:BACKGROUND_TRACK_GAMEPLAY_3];
        }
    else if (level > 5)
        {
        [SoundManager.instance playBackgroundTrack:BACKGROUND_TRACK_GAMEPLAY_2];
        }
    else
        {
        [SoundManager.instance playBackgroundTrack:BACKGROUND_TRACK_GAMEPLAY_1];
        }
    
    
}

- (void)createAccessaryArray:(TileType)type
{
//    if (type == k1x1) {
//        a1x1 = [NSMutableArray new];
//        for (int i = 0; i<3; i++) {
//            [a1x1 addObject:[NSNumber numberWithInt:i]];
//        }
//    } else if (type == k1x2) {
//        a1x2 = [NSMutableArray new];
//        for (int i = 0; i<4; i++) {
//            [a1x2 addObject:[NSNumber numberWithInt:i]];
//        }
//    } else if (type == k2x1) {
//        a2x1 = [NSMutableArray new];
//        for (int i = 0; i<4; i++) {
//            [a2x1 addObject:[NSNumber numberWithInt:i]];
//        }
//    }
}
- (void)createLevel
{
/*
    Tile1x1 *tile1x1 = [[Tile1x1 alloc] initWithPosition:0 andSprite:[self addAccessary:k1x1] andEffect:nil];
    [tile1x1 initAnimations:@"Tile2x1"];
    [sceneSpriteBatchNode1 addChild:tile1x1 z:kBodySpriteZValue tag:0];
    
    Tile1x1 *tile1x11 = [[Tile1x1 alloc] initWithPosition:45 andSprite:[self addAccessary:k1x1] andEffect:nil];
    [tile1x11 initAnimations:@"Tile1x2"];
    [sceneSpriteBatchNode1 addChild:tile1x11 z:kBodySpriteZValue tag:1];
*/
    //NSArray *level = [LevelHelper.instance getLevelMatrix:GameManager.instance.currentLevel];
    NSArray *level = [LevelHelper.instance getCSV];// [LevelHelper.instance getLevelMatrix:2];
    
    // fix last element issue remove "-1"
    for (int i = 0; i < level.count - 1; i++)
    {
        NSString * texture = [level objectAtIndex:i];
        if (![texture isEqualToString:@"-1"])
        {
            Layout *block1x1 = [[Layout alloc] initPosition:i withTexture:texture];
            [sceneSpriteBatchNode1 addChild:block1x1 z:kBodySpriteZValue tag:i];
        }
        
//        NSString *str = [level objectAtIndex:i];
//        NSArray *element = [str componentsSeparatedByString:@","];
//        int pos = [[element objectAtIndex:0] intValue];
//        NSString *type = [element objectAtIndex:1];
        /*
        if ([[level objectAtIndex:i] intValue] == b_0_0)
        {
            int texture = 1;//[[element objectAtIndex:2] intValue];
            
            Layout *block1x1 = [[Layout alloc] initWithPosition:i andTexture:b_0_0];
            //[block1x1 initAnimations:@"Block" andTexture:texture];
            [sceneSpriteBatchNode1 addChild:block1x1 z:kBodySpriteZValue tag:i];
        }
       
        if ([@"gold" isEqualToString:type])
        {
//            Tile1x1 *tile = [[Tile1x1 alloc] initWithPosition:tilePos andSprite:[self addAccessary:k1x1] andEffect:nil];
//            [tile1x111 initAnimations:@"Tile2x2"];
//            [sceneSpriteBatchNode1 addChild:tile1x111 z:kBodySpriteZValue tag:2];
            //Tile1x1 *tile1x1 = [[Tile1x1 alloc] initWithPosition:pos andSprite:[self addAccessary:k1x1] andEffect:[self addEffect:k1x1]];
            Tile1x1 *gold = [[Tile1x1 alloc] initWithPosition:pos andSprite:[self addAccessary:k1x1] andEffect:nil];
            [gold initAnimations:@"Tile2x2"];
            [sceneSpriteBatchNode1 addChild:gold z:kBodySpriteZValue tag:i];
        }
        else if ([@"block1x1" isEqualToString:type])
        {
            int texture = [[element objectAtIndex:2] intValue];

            Unit *block1x1 = [[Unit alloc] initWithPosition:pos];
            [block1x1 initAnimations:@"Block" andTexture:texture];
            [sceneSpriteBatchNode1 addChild:block1x1 z:kBodySpriteZValue tag:i];
        }
        else if ([@"red" isEqualToString:type] || [@"green" isEqualToString:type] || [@"blue" isEqualToString:type] || [@"white" isEqualToString:type])
        {
            Unit *sb = [[Unit alloc] initWithPosition:pos];
            [sb initAnimations:type];
            [sceneSpriteBatchNode1 addChild:sb z:kBodySpriteZValue tag:i];
        }
        */
    }
}

- (GameItem*)addAccessary:(TileType)type
{
    Accessary *item = [[Accessary alloc] init];
    [item initAnimations:@"A1"];
    [sceneSpriteBatchNode addChild:item z:kBodySpriteZValue tag:-1];
    return item;
//    if (type == k1x1)
//        {
//        return [self creat1x1accessary];
//        }
//    else if (type == k1x2)
//        {
//        return [self creat1x2accessary];
//        }
//    else
//        {
//        return [self creat2x1accessary];
//        }
//    return nil;
}

- (GameItem*)addEffect:(TileType)type
{
    GameItem *returnItem;
//    if (type == k1x1)
//        {
        Flash1x1 *item = [[Flash1x1 alloc] init];
        returnItem = item;
//        }
//    else if (type == k1x2)
//        {
//        return [self creat1x2accessary];
//        }
//    else
//        {
//        return [self creat2x1accessary];
//        }
    [sceneSpriteBatchNode addChild:returnItem z:kBodySpriteZValue tag:-1];
    return returnItem;
}

@end
