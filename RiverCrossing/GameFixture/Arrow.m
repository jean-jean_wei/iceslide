//
//  Son2.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-16.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "Arrow.h"

@implementation Arrow

-(void)checkAndClampSpritePosition
{
  //  [super checkAndClampSpritePosition];
}

//-(void)changeState:(CharacterStates)newState
//{
//    [self stopAllActions];
//    id action = nil;
//    //  self.characterState = newState;
//    
//    switch (newState)
//    {
//        case kStateIdle:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
//            break;
//            
//        case kStateWalking:            
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kWalkingAnim]];
//            break;
//            
//        case kStateCrouching:                        
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kCrouchingAnim]];
//            break;
//                        
//        case kStateBreathing:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
//            break;
//        case kStatePreIdle:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kPreIdlingAnim]];
//            self.characterState = kStateIdle;
//            break;
//            
//        default:
//            break;
//    }
//    if (action != nil)
//    {
//        [self runAction:action];
//    }
//}
//
//#pragma mark -
//-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects
//{
//
//    [self checkAndClampSpritePosition];
//    
//    
//    if ([self numberOfRunningActions] == 0)
//    {
//        if (self.characterState == kStatePreIdle)
//        {
//            [self changeState:kStateIdle];
//        }
//        else
//        {
//            [self changeState:self.characterState];
//        }
//
//        
//    }
//    
//}

#pragma mark -
-(void)initAnimations
{
    // load animations from Classname.plist to animation array
    NSString *str = NSStringFromClass([self class]);
    ccAnimArray = [NSMutableArray new];
    for (int i = 0; i<kNumOfCharacterAnimeTypes; i++)
    {
        CCAnimation *temp = [self loadAnimation:[ANIM_NAMES objectAtIndex:i] withPlist:str];
        [ccAnimArray addObject:temp];
    }
    
}

#pragma mark -
- (id)init
{
    self = [super init];
    if (self != nil)
    {
        self.gameObjectType = kBodyType;
        
        millisecondsStayingIdle = 0.0f;
        
        [self initAnimations];
        
        [self onExit];
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"arrow_1.png"]];
        self.characterState = kStateIdle;

        
    }
    return self;
}

@end
