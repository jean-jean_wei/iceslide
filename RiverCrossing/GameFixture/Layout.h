//
//  Layout.h
//  IceSliding
//
//  Created by Jean-Jean Wei on 2018-05-27.
//  Copyright © 2018 Ice Whale Inc. All rights reserved.
//

#import "GameObject.h"
#import "TileHelper.h"

#define LAYOUT [NSArray arrayWithObjects:@"b00", @"b01", @"b01", @"b01", @"b01", @"b01", @"b01", @"b01", @"b01", @"b01", nil]


#define SPECIAL_BLOCK [NSArray arrayWithObjects: @"solidAnim1x1", nil]
#define BLOCK_STONE_NAMES [NSArray arrayWithObjects: @"stoneAnim1x1_0", @"stoneAnim1x1_1", @"stoneAnim1x1_2", @"stoneAnim1x1_3",nil]




@interface Layout : GameObject
{
    
    NSMutableArray *ccAnimArray;
    int hitPoint;
    ccTime delta;
    CharacterStates characterState;
    float millisecondsStayingIdle;
    bool runAnime;
}
@property (assign) CharacterStates characterState;
@property (assign) int hitPoint;
@property (assign) bool runAnime;

- (id)initPosition:(int)pos withTexture:(NSString*)texture;

typedef enum
{
    empty = -1,
    
    b_0_0 = 12,
    b_0_1 = 16,
    b_0_2 = 20,
    b_0_3 = 24,
    b_0_4 = 28,
    b_0_5 = 32,
    
    b_1_0 = 34,
    b_1_1 = 35,
    b_1_2 = 36,
    b_1_3 = 37,
    
    b_2_0 = 38,
    b_2_1 = 39,
    b_2_2 = 40,
    b_2_3 = 41,
    
    b_3_0 = 42,
    b_3_1 = 43,
    b_3_2 = 44,
    b_3_3 = 45,
    
    b_4_0 = 46,
    b_4_1 = 47,
    b_4_2 = 48,
    b_4_3 = 49,
    
    b_5_0 = 50,
    b_5_1 = 51,
    b_5_2 = 52,
    b_5_3 = 53,
    
    b_6_0 = 54,
    b_6_1 = 55,
    b_6_2 = 56,
    b_6_3 = 57,
    
} BlockTypes;

@end
