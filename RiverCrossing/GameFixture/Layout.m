//
//  Layout.m
//  IceSliding
//
//  Created by Jean-Jean Wei on 2018-05-27.
//  Copyright © 2018 Ice Whale Inc. All rights reserved.
//

#import "Layout.h"
#import "BlockMapping.h"

@implementation Layout

@synthesize  hitPoint, runAnime, characterState;

-(void)checkAndClampSpritePosition
{
    
}



- (void)initAnimations:(NSString*)texture
{
    if ([texture isEqualToString:@"0"])
    {
        runAnime = true;
        // load animations from plist to animation array
        ccAnimArray = [NSMutableArray new];
//        for (int i = 0; i<LAYOUT.count; i++)
//        {
            CCAnimation *temp = [self loadAnimation:@"al" withPlist:@"LayoutAnime"];
            [ccAnimArray addObject:temp];
        //}
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:firstFrameName]];
    }
    else
    {
        runAnime = false;
        NSString *spriteName = [[TAXTURE_NAMES objectForKey:texture] stringByAppendingString:@".png"];
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:spriteName]];
    }
}

- (id)initPosition:(int)pos withTexture:(NSString*)texture
{
    self = [super init];
    if (self != nil)
    {
        self.gameObjectType = kBodyType;
        
        CGPoint p = [TileHelper.instance getPosition:pos];
        self.position = p;
        self.anchorPoint = ccp(0, 0);
        
        millisecondsStayingIdle = 0.0f;
        
        [self initAnimations:texture];
        
        self.characterState = kStateIdle;
        [self onExit];
    }
    return self;
}

#pragma mark -
-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects
{
    [self checkAndClampSpritePosition];
    
    if ([self numberOfRunningActions] == 0)
    {

            [self changeState:kStateIdle];
        
        
    }
}

-(void)changeState:(CharacterStates)newState
{
    if (!runAnime) return;
    
    [self stopAllActions];
    id action = nil;
    //  self.characterState = newState;
    
//    switch (newState)
//    {
//        case kStateIdle:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
//            break;
            
//        case kStateWalking:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kWalkingAnim]];
//            break;
//
//        case kStateTouched:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kTouchedAnim]];
//            break;
//
//        case kStateBreathing:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
//            break;
//
//        case kStatePreIdle:
//            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kPreIdlingAnim]];
//            self.characterState = kStateIdle;
//            break;
//
//        default:
//            break;
//    }
    if (action != nil)
    {
        [self runAction:action];
    }
}

@end
