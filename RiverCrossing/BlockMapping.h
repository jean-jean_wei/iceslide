//
//  BlockMapping.h
//  IceSliding
//
//  Created by Jean-Jean Wei on 2017-07-04.
//  Copyright © 2017 Ice Whale Inc. All rights reserved.
//

#ifndef BlockMapping_h
#define BlockMapping_h


#define BlocksDictionary @{@"-1" : @"empty"\
@"12" : @"b_0_0"\
@"16" : @"b_0_1"\
}

#define TAXTURE_NAMES [NSDictionary dictionaryWithObjectsAndKeys: @"al_0", @"-1",\
@"b_0_0", @"12", \
@"b_0_1", @"16", \
@"b_0_2", @"20", \
@"b_0_3", @"24", \
@"b_0_4", @"28", \
@"b_0_5", @"32", \
@"b_1_0", @"34", \
@"b_1_1", @"35", \
@"b_1_2", @"36", \
@"b_1_3", @"37",\
@"b_2_0", @"38", \
@"b_2_1", @"39", \
@"b_2_2", @"40",\
@"b_2_3", @"41", \
@"b_3_0", @"42", \
@"b_3_1", @"43", \
@"b_3_2", @"44", \
@"b_3_3", @"45", \
@"b_4_0", @"46", \
@"b_4_1", @"47", \
@"b_4_2", @"48", \
@"b_4_3", @"49", \
@"b_5_0", @"50", \
@"b_5_1", @"51", \
@"b_5_2", @"52", \
@"b_5_3", @"53", \
@"b_6_0", @"54", \
@"b_6_1", @"55", \
@"b_6_2", @"56", \
@"b_6_3", @"57", \
nil]

#define empty = "-1";
#define b_0_0 = "0";
#define b_0_1 = "9";

#define b_1_0 = "18";
#define b_1_1 = "1";
#define b_1_2 = "10";
#define b_1_3 = "19";

#define b_2_0 = "2";
#define b_2_1 = "11";
#define b_2_2 = "20";
#define b_2_3 = "3";

#define b_3_0 = "12";
#define b_3_1 = "21";
#define b_3_2 = "4";
#define b_3_3 = "13";

#define b_4_0 = "22";
#define b_4_1 = "5";
#define b_4_2 = "14";
#define b_4_3 = "23";

#define b_5_0 = "6";
#define b_5_1 = "15";
#define b_5_2 = "24";
#define b_5_3 = "7";

#define b_6_0 = "16";
#define b_6_1 = "25";
#define b_6_2 = "8";
#define b_6_3 = "17";

#endif /* BlockMapping_h */
