//
//  Tile1x1.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-03.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "Tile1x1.h"
#import "Unit.h"

@implementation Tile1x1

-(void)checkAndClampSpritePosition
{
    [super checkAndClampSpritePosition];
}

-(void)applyTimeDelta:(float)deltaTime
{
    oldPosition = self.position;
    CGPoint newPosition = oldPosition;
    
    //CGPoint velocity = CGPointMake(1, 1);
    CGPoint scaledVelocity;
    if (dSqrt >= 60) {
        scaledVelocity = ccpMult(velocity, SPEED_FAST);
    } else {
        scaledVelocity = ccpMult(velocity, SPEED_SLOW);
    }
    
    //float distance = scaledVelocity.x * deltaTime;
    
    if (degrees == 90 || (degrees == 270))
    {
        float distance = scaledVelocity.y * deltaTime;
           //NSLog(@"distance = %f",distance);
        newPosition.y+=distance;
        
    }
    else
    {
        float distance = scaledVelocity.x * deltaTime;
        newPosition.x+=distance;
       
        //NSLog(@"distance = %f",distance);
    }
    
    //NSLog(@"distance = %f",distance);
    
    self.position = newPosition;
    accessary.position = newPosition;
    if (effects)
    {
        effects.position = [self getEffectPos:newPosition];
    }
}


-(void)changeState:(CharacterStates)newState
{
    [self stopAllActions];
    id action = nil;
    self.characterState = newState;
    //NSLog(@">>> kState = %i", newState);
    switch (newState)
    {
        case kStateIdle:
            if (effects)
            {
                [effects changeState:kStateIdle];
            }
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kIdlingAnim]];
            break;
            
        case kStateWalking: 
            accessary.characterState = kStateWalking;
            if (effects)
            {
                effects.characterState = kStateWalking;
            }
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kWalkingAnim]];
            break;
            
        case kStateTouched:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kTouchedAnim]];
            break;
            
        case kStateBreathing:
            if (effects)
            {
                [effects changeState:kStateBreathing];
            }
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
            break;
            
        case kStatePreIdle:
            [accessary changeState:kStatePreIdle];
            if (effects)
            {
                [effects changeState:kStatePreIdle];
            }
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kPreIdlingAnim]];
            break;
            
        default:
            break;
    }
    if (action != nil)
    {
        [self runAction:action];
    }
}

#pragma mark -
-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects
{
    // Check for collisions
    // Change this to keep the object count from querying it each time
    if (characterIsPicked && self.characterState == kStateWalking)
    {
        if (specialBlock != nil)
        {
            CGPoint newPos = self.position;
            if (degrees == 90)
            {
                newPos.y = specialBlock.position.y + specialBlock.boundingBox.size.height;
            }
            else if (degrees == 270)
            {
                newPos.y = specialBlock.position.y - specialBlock.boundingBox.size.height;
            }
            else if (degrees == 180)
            {
                newPos.x = specialBlock.position.x - specialBlock.boundingBox.size.width;
            }
            else
            {
                newPos.x = specialBlock.position.x + specialBlock.boundingBox.size.width;
            }
            self.position = newPos;
            accessary.position = self.position;
            if (effects)
            {
                effects.position = [self getEffectPos:self.position];
            }
            specialBlock.isActive = true;
            specialBlock.visible = true;
            specialBlock = nil;
        }
        
        CGRect myBoundingBox = [self boundingBox];
        for (GameObject *character in listOfGameObjects)
        {
            // No need to check collision with one's self
            
            if (character.tag != self.tag && self.tag != kSpriteToDestoryTagValue && character.isActive)
            {
                CGRect characterBox = [character boundingBox];
#ifdef DEBUG
                NSLog(@"myBoundingBox = %@", NSStringFromCGRect(myBoundingBox));
                NSLog(@"characterBox = %@", NSStringFromCGRect(characterBox));
#endif
                if (CGRectIntersectsRect(myBoundingBox, characterBox))
                {                    
                    if (character.gameObjectTag == kSpecialBlock)
                    {
                        int state = [(Unit*)character checkSpecialBlockState];
                        if (state == 2)
                        {
                            self.tag = kSpriteToDestoryTagValue;
                            accessary.tag = kSpriteToDestoryTagValue;
                        }
                        else if (state == 1)
                        {
                            CGPoint newPos = self.position;
                            if (degrees == 90)
                            {
                                newPos.y = character.position.y + characterBox.size.height;
                            }
                            else if (degrees == 270)
                            {
                                newPos.y = character.position.y - characterBox.size.height;
                            }
                            else if (degrees == 180)
                            {
                                newPos.x = character.position.x - characterBox.size.width;
                            }
                            else
                            {
                                newPos.x = character.position.x + characterBox.size.width;
                            }
                            self.position = newPos;
                            break;
                        }
                        else if (state == 3)
                        {
                            if (character.isActive)
                            {
                                [SoundManager.instance playSoundEffect:@"collision"];
                                [self stopAllActions];
                                
                                [self changeState:kStatePreIdle];
                                self.position = character.position;
                                accessary.position = self.position;
                                if (effects)
                                {
                                    effects.position = [self getEffectPos:self.position];
                                }
                                character.isActive = false;
                                character.visible = false;
                                specialBlock = character;
                                GameManager.instance.movingInProgress = NO;
                                characterIsPicked = NO;
                                break;
                            }

                        }
                    }
                    
                    [SoundManager.instance playSoundEffect:@"collision"];
                    [self stopAllActions];
                    
                    [self changeState:kStatePreIdle];
                    
                    
                    if (!isnan(self.position.x) && !isnan(self.position.y))
                    {
                        CGPoint pos = [TileHelper.instance getClosestPos:self.boundingBox withDegree:degrees andOtherBox:characterBox];
                                                NSLog(@"Currected pos = %@", NSStringFromCGPoint(pos));
                        self.position = pos;
                        accessary.position = pos;
                        if (effects)
                        {
                            effects.position = [self getEffectPos:pos];
                        }
                    }
                    else
                    {
                        self.position = oldPosition;
                        accessary.position = oldPosition;
                        if (effects)
                        {
                            effects.position = [self getEffectPos:oldPosition];
                        }
                    }
                    GameManager.instance.movingInProgress = NO;
                    characterIsPicked = NO;
                }
            }
        }
    }

    [self checkAndClampSpritePosition];
    
    if (self.characterState == kStateWalking)
    {
         [self applyTimeDelta:deltaTime];
    }
    
    if ([self numberOfRunningActions] == 0)
    {
        if (self.characterState == kStatePreIdle)
        {
            [self changeState:kStateIdle];
            characterIsPicked = NO;
            GameManager.instance.movingInProgress = NO;
        }
        else
        {
            [self changeState:self.characterState];
        }
        
    }
    
}

#pragma mark -
- (void)initAnimations:(NSString*)plistName
{
    // load animations from plist to animation array
    ccAnimArray = [NSMutableArray new];
    for (int i = 0; i<kNumOfCharacterAnimeTypes; i++)
    {
        CCAnimation *temp = [self loadAnimation:[ANIM_NAMES objectAtIndex:i] withPlist:plistName];
        [ccAnimArray addObject:temp];
    }
    [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:firstFrameName]];
}

#pragma mark -

- (id)initWithPosition:(int)pos andSprite:(id)obj andEffect:(GameItem*)effect
{
    
    self = [super init];
    if (self != nil)
    {
        specialBlock = nil;
        CGPoint p = [TileHelper.instance getPosition:pos];
        self.position = p;
        self.anchorPoint = ccp(0, 0);
        oldPosition = p;
        accessary = obj;
        accessary.position = p;
        accessary.anchorPoint = ccp(0, 0);
        
        
        if (effect)
        {
            effects = effect;
            effects.position = [self getEffectPos:p];
        }
        millisecondsStayingIdle = 0.0f;
        
    }
    return self;
}


@end
