//
//  GameSceneTutorial.m
//  IceSliding
//
//  Created by Jean-Jean Wei on 2016-12-14.
//  Copyright © 2016 Ice Whale Inc. All rights reserved.
//

#import "GameSceneTutorial.h"
#import "SubMenuLayer.h"

@implementation GameSceneTutorial

-(id)init {
    self = [super init];
    if (self != nil) {
        // Background Layer
        BackgroundLayer *backgroundLayer = [[BackgroundLayer alloc] initWithType:kGameLevel1Background]; // 1
        CGSize screenSize = [CCDirector sharedDirector].winSize;
        if (screenSize.height == 480.0f ) {
            CGPoint p = backgroundLayer.position;
            backgroundLayer.position = ccp(p.x, p.y-44);
        }
        [self addChild:backgroundLayer z:0]; // 2
        
        // Gameplay Layer
        GameplayLayerTutorial *gameplayLayer = [GameplayLayerTutorial node]; // 3
        [self addChild:gameplayLayer z:5]; // 4
        // Gameplay SubLayer
//        SubMenuLayer *subMenuLayer = [SubMenuLayer node]; 
//        [subMenuLayer setLowerLayer:gameplayLayer];
//        [self addChild:subMenuLayer z:6];
        
    }
    return self;
}

@end
