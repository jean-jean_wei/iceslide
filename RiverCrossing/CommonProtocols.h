//
//  Header.h
//  ro3
//
//  Created by JJ WEI on 12-06-27.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

typedef enum 
{
    kGameObjectNormal,
    kAccessaryTag,
    kSpecialBlock,
    kNumOfGameObjectTags
} GameObjectTag;




// Tile helper
typedef enum {
    k2x2=0,
    k1x1=1,
    k1x2=2,
    k2x1=3
    
} TileType;

// Tile helper
typedef enum {
    k1x1_1=101,
    k1x1_2,
    k1x1_3,
    k1x2_1=201,
    k1x2_2,
    k2x1_1=301
    
} AccessaryType;

// Tile helper
typedef enum {
    up=0,
    down=1,
    left=2,
    right=3
    
} Directions;


typedef enum {
    kStateIdle,
    kStateBreathing,
    kStateWalking,
    kStateTouched,
    kStatePreIdle
    
} CharacterStates; // 1


typedef enum
{
    kBreathingAnim,
    kWalkingAnim,
    kTouchedAnim,
    kIdlingAnim,
    kPreIdlingAnim,
    kNumOfCharacterAnimeTypes
} CharacterAnimTypes;

typedef enum {
    kObjectTypeNone,
    kBodyType,
    kSkullType,
    kRockType,
    kMeteorType,
    kFrozenBodyType,
    kIceType,
    kLongBlockType,
    kCartType,
    kSpikesType,
    kDiggerType,
    kGroundType
} GameObjectType;

@protocol GameplayLayerDelegate
-(void)createObjectOfType:(GameObjectType)objectType
               withHealth:(int)initialHealth
               atLocation:(CGPoint)spawnLocation
               withZValue:(int)ZValue;

@end

