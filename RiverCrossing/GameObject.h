//
//  GameObject.h
//  ro3
//
//  Created by JJ WEI on 12-06-27.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"
//#import "Constants.h"
//#import "CommonProtocols.h"

@interface GameObject : CCSprite
{
    BOOL isActive;
    BOOL reactsToScreenBoundaries;
    int tag;
    CGSize screenSize;
    GameObjectType gameObjectType;
    GameObjectTag gameObjectTag;
    NSString *firstFrameName;
    NSMutableArray *eachStateFirstFrameName;
}

@property (assign) BOOL isActive;
@property (assign) BOOL reactsToScreenBoundaries;
@property (assign) CGSize screenSize;
@property (assign) GameObjectType gameObjectType;
@property (assign) GameObjectTag gameObjectTag;


- (void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects;

- (CCAnimation*)loadAnimation:(NSString*)animationName withPlist:(NSString*)plistName;

@end
