//
//  Unit.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-16.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameFixture.h"
#define SPECIAL_BLOCK [NSArray arrayWithObjects: @"solidAnim1x1", nil]
#define BLOCK_STONE_NAMES [NSArray arrayWithObjects: @"stoneAnim1x1_0", @"stoneAnim1x1_1", @"stoneAnim1x1_2", @"stoneAnim1x1_3",nil]
@interface Unit : GameFixture
{
//    NSMutableArray *ccAnimArray;
//    CharacterAnimTypes *ccAnimTypes;
    float millisecondsStayingIdle; 
}

- (id)initWithPosition:(int)pos;
- (void)initAnimations:(NSString*)plistName andTexture:(int)texture;
- (void)initAnimations:(NSString*)plistName;

- (int)checkSpecialBlockState;
@end
