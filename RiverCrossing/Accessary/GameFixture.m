//
//  GameFixture.m
//  IceSliding
//
//  Created by Jean-Jean Wei on 2017-01-02.
//  Copyright © 2017 Ice Whale Inc. All rights reserved.
//

#import "GameFixture.h"

@implementation GameFixture



@synthesize fixtureState, hitPoint;



-(void)checkAndClampSpritePosition
{
    
}

-(void)changeState:(FixtureStates)newState
{
    [self stopAllActions];
    id action = nil;
    self.fixtureState = newState;
    
    switch (newState)
    {
        case kNormal:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
            break;
            
        case kRed:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
            
            break;
            
        case kWhite:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
            break;
            
        case kGreen:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
            break;
            
        case kBlue:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:0]];
            break;
            
        default:
            break;
    }
    if (action != nil)
    {
        [self runAction:action];
    }
}

#pragma mark -
-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects
{
    [self checkAndClampSpritePosition];
    
    if ([self numberOfRunningActions] == 0)
    {
        [self changeState:self.fixtureState];
    }
}

@end
