//
//  Son1.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-16.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "Flash1x1.h"

@implementation Flash1x1

-(void)checkAndClampSpritePosition
{
  //  [super checkAndClampSpritePosition];
}


#pragma mark -
-(void)initAnimations
{
    // load animations from Classname.plist to animation array
    NSString *str = NSStringFromClass([self class]);
    ccAnimArray = [NSMutableArray new];
    for (int i = 0; i<kNumOfCharacterAnimeTypes; i++)
    {
        CCAnimation *temp = [self loadAnimation:[ANIM_NAMES objectAtIndex:i] withPlist:str];
        [ccAnimArray addObject:temp];
    }
    
}

#pragma mark -
- (id)init
{
    self = [super init];
    if (self != nil)
    {
        self.gameObjectType = kBodyType;
        
        millisecondsStayingIdle = 0.0f;
        
        [self initAnimations];
        [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"son1_idle_1.png"]];
        self.characterState = kStateIdle;

        
    }
    return self;
}

@end
