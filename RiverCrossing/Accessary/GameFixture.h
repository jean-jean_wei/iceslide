//
//  GameFixture.h
//  IceSliding
//
//  Created by Jean-Jean Wei on 2017-01-02.
//  Copyright © 2017 Ice Whale Inc. All rights reserved.
//

#import "GameObject.h"
#import "TileHelper.h"

typedef enum
{
    kNormal,
    kRed,
    kWhite,
    kGreen,
    kBlue,
} FixtureStates;

@interface GameFixture : GameObject
{
    NSMutableArray *ccAnimArray;
    FixtureStates fixtureState;
    int hitPoint;
    ccTime delta;
}

@property (assign) FixtureStates fixtureState;
@property (assign) int hitPoint;
- (void)changeState:(FixtureStates)newState;

@end
