//
//  Unit.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-16.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "Unit.h"

@implementation Unit

-(void)checkAndClampSpritePosition
{
  //  [super checkAndClampSpritePosition];
}

- (int)checkSpecialBlockState
{
    int returnState = -1;
    
    switch (self.fixtureState)
    {
        case kRed:
            returnState = 0;
            hitPoint--;
            if (hitPoint < 0)
            {
                self.tag = kSpriteToDestoryTagValue;
            }
            break;
            
        case kWhite:
            returnState = 1;
            hitPoint--;
            if (hitPoint < 0)
            {
                ccAnimArray = [NSMutableArray new];
                for (int i = 0; i<SPECIAL_BLOCK.count; i++)
                {
                    CCAnimation *temp = [self loadAnimation:[SPECIAL_BLOCK objectAtIndex:i] withPlist:@"green"];
                    [ccAnimArray addObject:temp];
                }
                 [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:firstFrameName]];
            [self changeState:kGreen];
                //[self changeState:kBlue];
            }
            break;
            
        case kGreen:
            returnState = 2;
            break;
            
        case kBlue:
            returnState = 3;
            break;
            
        default:
            break;
    }
    return returnState;
}

#pragma mark -
- (void)initAnimations:(NSString*)plistName andTexture:(int)texture
{
    // load animations from plist to animation array
    ccAnimArray = [NSMutableArray new];
//    for (int i = 0; i<BLOCK_ANIM_NAMES.count; i++)
//    {
        CCAnimation *temp = [self loadAnimation:[BLOCK_STONE_NAMES objectAtIndex:texture] withPlist:plistName];
        [ccAnimArray addObject:temp];
    //}
    [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:firstFrameName]];
}

- (void)initAnimations:(NSString*)plistName
{
    gameObjectTag = kSpecialBlock;
    if ([plistName isEqualToString:@"red"])
    {
        self.fixtureState = kRed;
        hitPoint = 3;
    }
    else if ([plistName isEqualToString:@"white"])
    {
        self.fixtureState = kWhite;
        hitPoint = 3;

    }
    else if ([plistName isEqualToString:@"green"])
    {
        self.fixtureState = kGreen;
    }
    else if ([plistName isEqualToString:@"blue"])
    {
        self.fixtureState = kBlue;
    }
    // load animations from plist to animation array
    ccAnimArray = [NSMutableArray new];
    for (int i = 0; i<SPECIAL_BLOCK.count; i++)
    {
        CCAnimation *temp = [self loadAnimation:[SPECIAL_BLOCK objectAtIndex:i] withPlist:plistName];
        [ccAnimArray addObject:temp];
    }
    [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:firstFrameName]];
}

#pragma mark -
- (id)initWithPosition:(int)pos
{
    self = [super init];
    if (self != nil)
    {
        self.gameObjectType = kBodyType;
        
        CGPoint p = [TileHelper.instance getPosition:pos];
        self.position = p;
        self.anchorPoint = ccp(0, 0);
        
        millisecondsStayingIdle = 0.0f;

        self.fixtureState = kNormal;
        [self onExit];
    }
    return self;
}

@end
