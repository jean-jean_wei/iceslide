//
//  TileHelper.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-22.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "TileHelper.h"

@implementation TileHelper

//Singleton Setup
+ (TileHelper*)instance
{
    static dispatch_once_t pred = 0;
    __strong static TileHelper *_instance = nil;
    
    dispatch_once(&pred, ^{
        _instance = [[self alloc] init];
        
    });
    return _instance;
    
}



-(id)init
{
    self = [super init];
    if (self != nil)
    {
        // TileHelper initialized
        CCLOG(@"TileHelper Singleton, init");
        gridArray = [self create_grid];

    }
    return self;
}

- (CGPoint)getClosestPos:(CGRect)myBox withDegree:(float)degrees andOtherBox:(CGRect)otherBox
{
    int idx = -1;
    float distance = 46001.00;
    CGPoint pos = myBox.origin;
    CGPoint otherPos = otherBox.origin;
    bool upDown = degrees == 270 || degrees == 90 ? true : false;


    for (int i = 0; i < gridArray.count; i++)
    {
        CGPoint t = [[gridArray objectAtIndex:i] CGPointValue];
        if ((upDown && t.x == pos.x) || (!upDown && t.y == pos.y))
        {
            if ((degrees == 0 && t.x < otherPos.x) || (degrees == 180 && t.x > otherPos.x) ||
            (degrees == 90 && t.y < otherPos.y) || (degrees == 270 && t.y > otherPos.y))
            {
                float tmp = upDown ?  ABS(t.y - pos.y) : ABS(t.x - pos.x);
                myBox.origin = t;
                if (CGRectIntersectsRect(myBox, otherBox))
                {
                    NSLog(@"Collision >>>>>");
                    
                }
                else
                {
                    
                    NSLog(@"myBox = %@", NSStringFromCGRect(myBox));
                    NSLog(@"otherBox = %@", NSStringFromCGRect(otherBox));
                }
                
                if (tmp < distance && !CGRectIntersectsRect(myBox, otherBox))
                {
                    

                    NSLog(@"distance  = %f, temp = %f", distance, tmp);
                    distance = tmp;
                    idx = i;
                    
                }
            }
        }
    }
    if (idx == -1)
    {
        return pos;
    } else {
        return [[gridArray objectAtIndex:idx] CGPointValue];
    }
}

- (CGPoint)getPosition:(int)idx;
{
    CGPoint p;
    p = [[gridArray objectAtIndex:idx] CGPointValue];
    return p;
}

- (NSArray*)create_grid
{
//#define RIGHT [[CCDirector sharedDirector] winSize].width - 12.0f
//#define TOP  [[CCDirector sharedDirector] winSize].height - 12.0f
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    NSMutableArray *temp = [NSMutableArray new];
    
    
    float offSet = (winSize.width - 11 - 12) / 22;
    float off = 16;
    //float offX = 13.0f;
    
    float startX = 11;
    float startY = winSize.height - off;
    //float offY = 13.0f;
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        off = 30;
        startX = 54;
        if (winSize.width == 1024)
        {
            off = 40;
            startX = 72;
        }
    }
    
    for (int i=0; i<27; i++)
    {
        float numY = startY - off * i;
        for (int j=0; j<22; j++)
        {
            float numX = startX + off * j;
            [temp addObject:[NSValue valueWithCGPoint:CGPointMake(numX, numY)]];
        }
    }
    return temp;
}


@end
