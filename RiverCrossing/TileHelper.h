//
//  TileHelper.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-22.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TileHelper : NSObject
{
    NSArray *gridArray;
}

+ (TileHelper*)instance;
 
- (CGPoint)getClosestPos:(CGRect)myBox withDegree:(float)degrees andOtherBox:(CGRect)otherBox;

- (CGPoint)getPosition:(int)idx;
- (NSArray*)create_grid;
@end
